import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SharedService } from '../services/shared';
import { QueryService } from '../services/query';

@Component({
  selector: 'cv-data',
  templateUrl: './cv-data.component.html',
  styleUrls: ['./cv-data.component.scss']
})
export class CvDataComponent implements OnInit {

  id;
  dataSource = [{s: 'prova s', g: 'prova g', o: 'prova o'}];
  keys = [];
  person : any;

  constructor(private route: ActivatedRoute, 
    private router: Router, 
    private sharedService: SharedService, private queryService: QueryService) {

   }

  ngOnInit(): void {

    //this.id = this.navigationEnd.url.match(/#(.*)/)[1];
    //var query = "SELECT * FROM NAMED <http://cristoforiHromei.it/cv/data> { GRAPH ?grafo {?soggetto rdf:type ?oggetto } }";

    this.initialize();

  }


  initialize(){
    this.person = {};
    this.route.params.subscribe(params => {
      console.log('id') //log the entire params object
      console.log(params['id']) //log the value of id
      this.id = params['id'];
      this.person = {};
      this.person['name'] = this.id;

      this.queryService.searchResource(':' + this.person['name'], '?p', '?o').subscribe(
          data => {
              console.log(data);
              data.forEach(item => {
                  let p = item['p']['value'];
                  let o = item['o']['value'];

                  if( item['p']['value'].search('#') != -1 ){
                      p = p.match(/#(.*)/)[1];
                  }
                  if( item['o']['value'].search('#') != -1 ){
                      o = o.match(/#(.*)/)[1];
                  }
                  this.person[p] = o.toLowerCase();
              });
              console.log(this.person);
          }, err => {
              console.log(err);
          }
      );

      this.queryService.searchResource('?s', ':aboutPerson', ':' + this.person['name']).subscribe(
        data => {
          data.forEach(item => {
            let s = item['s']['value'];

            if( item['s']['value'].search('#') != -1 ){
                s = s.match(/#(.*)/)[1];
            }
            this.person['curriculum'] = {};
            this.person['curriculum']['id'] = s;
            //this.person['curriculum']['hasEducation'] = [];
            //this.person['curriculum']['hasSkill'] = [];
            //this.person['curriculum']['HasCourse'] = [];

            this.queryService.searchResource(':' + s, '?p', '?o').subscribe(
              data => {

                  data.forEach(item => {
                    let p = item['p']['value'];
                    let o = item['o']['value'];

                    if( p.search('#') != -1 ){
                        p = p.match(/#(.*)/)[1];
                    }
                    if( o.search('#') != -1 ){
                        o = o.match(/#(.*)/)[1];
                    }

                    //if(p == 'hasEducation'){
                    //  this.getEducations(o);
                    //} else if(p == 'hasSkill'){
                    //  this.getSkill(o);
                    if(this.person['curriculum'][p] == undefined){
                      this.person['curriculum'][p] = [];
                    }
                    if(item['o']['termType'] == 'NamedNode'){
                      this.getNode(p, o);
                    } else {
                      this.person['curriculum'][p] = o;
                    }

                  });
                  console.log('after curriculum');
                  console.log(this.person);

              }, err => {
                console.log(err);
              }
            );

          });

        }, err => {
          console.log(err);
        }
      );

    });
  }

  getNode(predicate, node){
    this.queryService.searchResource(':' + node, '?p', '?o').subscribe(
      data => {

        let obj = {'id': node};

        data.forEach(item => {
          let p = item['p']['value'];
          let o = item['o']['value'];

          if( p.search('#') != -1 ){
              p = p.match(/#(.*)/)[1];
          }
          if( o.search('#') != -1 ){
              o = o.match(/#(.*)/)[1];
          }

          obj[p] = o;
          
        });

        this.person['curriculum'][predicate].push(obj);

      }, err => {
        console.log(err);
      }
    );
  }


  searchPerson(person){
    this.router.navigate(['/cv/data/', person]);
  }

}

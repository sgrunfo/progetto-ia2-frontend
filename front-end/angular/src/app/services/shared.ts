import { Injectable } from '@angular/core';

@Injectable()
export class SharedService { 
    url = 'http://cristoforiHromei.it/cv/data';
    graph = '<http://cristoforiHromei.it/cv/data>';
    rdf = '<http://www.w3.org/1999/02/22-rdf-syntax-ns#>';
    rdfs = '<http://rdfs.org/resume-rdf/cv.rdfs#>';
    data = '<http://cristoforiHromei.it/cv/data#>';

    constructor() {}

    getUrl(){
        return this.url;
    }

    getRdf(){
        return this.rdf;
    }

    getGraph(){
        return this.graph;
    }

    getRdfs(){
        return this.rdfs;
    }
    
    getData(){
        return this.data;
    }
}
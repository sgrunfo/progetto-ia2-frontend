import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { map } from 'rxjs/operators';
import { SharedService } from './shared';

@Injectable()
export class QueryService { 
    server = 'http://127.0.0.1:13000/api/';

    constructor(private http: Http, private shared: SharedService) {}

    rawQuery(query){
        const headers = new Headers();
        var api = 'raw_query/select';
        var response = this.http.post(this.server + api, 
        { 
            query: query,
        }, 
        {
            headers: headers
        })
        .pipe(map( response => response.json() ));
        return response;
    }

    searchResource(sogg = '?sogg', pred = '?pred', ogg = '?ogg'){
        const headers = new Headers();
        var api = 'raw_query/select';
        var query = 'PREFIX rdf:' + this.shared.getRdf() + ' ';
        query += 'PREFIX rdfs:' + this.shared.getRdfs() + ' ';
        query += 'PREFIX :' + this.shared.getData() + ' ';
        query += 'SELECT * ';
        //query += 'FROM NAMED ' + this.shared.getGraph(); + ' ';
        query += 'WHERE { ' + sogg + ' ' + pred + ' ' + ogg + ' }';

        var response = this.http.post(this.server + api, 
        { 
            query: query,
        }, 
        {
            headers: headers
        })
        .pipe(map( response => response.json() ));
        return response;
    }

    searchResource2(array){
        const headers = new Headers();
        var api = 'raw_query/select';
        var query = 'PREFIX rdf:' + this.shared.getRdf() + ' ';
        query += 'PREFIX rdfs:' + this.shared.getRdfs() + ' ';
        query += 'PREFIX :' + this.shared.getData() + ' ';
        query += 'SELECT * ';
        //query += 'FROM NAMED ' + this.shared.getGraph(); + ' ';
        query += 'WHERE { ' + array[0][0] + ' ' + array[0][1] + ' ' + array[0][2] + ' .';
        query += ' ' + array[1][0] + ' ' + array[1][1] + ' ' + array[1][2]; 
        query += ' }';

        var response = this.http.post(this.server + api, 
        { 
            query: query,
        }, 
        {
            headers: headers
        })
        .pipe(map( response => response.json() ));
        return response;
    }

    addPerson(id, givenName, familyName, birthDate, birthPlace = '', gender, hasCitizenship = '', hasDriversLicense = 'false', noOfChildren = 0, maritalStatus = '', hasNationality = '', image = ''){
        const headers = new Headers();
        var api = 'person';

        var response = this.http.post(this.server + api, 
            { 
                id: id,
                givenName: givenName,
                familyName: familyName,
                birthDate: birthDate,
                birthPlace: birthPlace,
                gender: gender,
                hasCitizenship: hasCitizenship,
                hasDriversLicense: hasDriversLicense,
                noOfChildren: noOfChildren,
                maritalStatus: maritalStatus,
                hasNationality: hasNationality,
                image: image

            }, 
            {
                headers: headers
            })
            .pipe(map( response => response.json() ));
            return response;
    
    }

    addCV(person_id, id, active, confidential, copyright, description, title, educations, courses, skills ){
        const headers = new Headers();
        var api = 'curricula';

        var response = this.http.post(this.server + api, 
            { 
                person: person_id,
                id: id,
                active: active,
                confidential: confidential,
                copyright: copyright,
                description: description,
                title: title,
                educations: educations,
                courses: courses,
                skills: skills

            }, 
            {
                headers: headers
            })
            .pipe(map( response => response.json() ));
            return response;
    }


}
//http://cristofori_hromei.it/cv/data#Lorenzo_Cristofori_CV
import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import { FormBuilder } from '@angular/forms';
import { QueryService } from '../services/query';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    persons = [];
    persons_array = [];
    filteredPersons = [];
    filteredOptions: Observable<string[]>;
    

    constructor(private formBuilder: FormBuilder, 
        private queryService: QueryService,
        private router: Router) {
    }

    ngOnInit() {

        this.queryService.searchResource('?sogg', 'rdf:type', ':Person').subscribe(
            data => {
                console.log(data);
                data.forEach(element => {
                    let resource = element['sogg']['value'];
                    resource = resource.match(/#(.*)/)[1]; 
                    this.persons.push( { name: resource } );
                    this.persons_array.push({'name': resource});
                });
                console.log(this.persons_array);

                this.persons_array.forEach(person => {

                    this.queryService.searchResource(':' + person['name'], '?p', '?o').subscribe(
                        data => {
                            console.log(data);
                            data.forEach(item => {
                                let p = item['p']['value'];
                                let o = item['o']['value'];

                                if( item['p']['value'].search('#') != -1 ){
                                    p = p.match(/#(.*)/)[1];
                                    //console.log(p);
                                }
                                if( item['o']['value'].search('#') != -1 ){
                                    o = o.match(/#(.*)/)[1];
                                    //console.log(o);
                                }
                                person[p] = o.toLowerCase();;
                            });
    
                        }, err => {
                            console.log(err);
                        }
                    );

                });

                this.filteredPersons = this.persons_array;

                console.log('person array:');
                console.log(this.persons_array);

            }, err => {
                console.log(err);
            }
        );

    }

    filterPersons(event) {
        //console.log(event);
        this.filteredPersons = [];

        this.persons_array.forEach(person => {
            let s = event.toLowerCase().replace(/ /g, "_");
            if(person.givenName){
                if(
                    person.givenName.toLowerCase().indexOf(s) != -1 ||
                    person.familyName.toLowerCase().indexOf(s) != -1 ||
                    person.name.toLowerCase().indexOf(s) != -1 ) {
                    this.filteredPersons.push(person);
                }
            }
        });

    }

    searchPerson(person){
        this.router.navigate(['/cv/data/', person.name]);
    }

}
  


/* 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> SELECT * FROM NAMED <http://cristoforiHromei.it/cv/data> { GRAPH ?grafo {?soggetto rdf:type ?oggetto } }
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> SELECT * WHERE  {?soggetto rdf:type ?oggetto } 
*/
import { Component, OnInit } from '@angular/core';
import { QueryService } from '../services/query';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'sparql',
  templateUrl: './sparql.component.html',
  styleUrls: ['./sparql.component.scss']
})
export class SparqlComponent implements OnInit {

  queryForm;
  dataSource = [{s: 'prova s', g: 'prova g', o: 'prova o'}];
  displayedColumns: string[] = ['s', 'g', 'o'];
  myControl = new FormControl();
  keys = [];
  queryResults = [];

  constructor(private queryService: QueryService,
    private formBuilder: FormBuilder,) { 

    this.queryForm = this.formBuilder.group({
      query: ''
    });

  }

  ngOnInit(): void {
  }

  runQuery(value){
    console.log(value);
    this.queryService.rawQuery(value.query).subscribe(
        data => {
            this.dataSource = data;

            this.keys = Object.keys(this.dataSource[0]);

            console.log(this.dataSource);
        }, err => {
            console.log(err);
        }
    )

}

}

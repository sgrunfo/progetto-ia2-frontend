import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { QueryService } from '../services/query';
import { Router } from '@angular/router';

@Component({
  selector: 'new-cv',
  templateUrl: './new-cv.component.html',
  styleUrls: ['./new-cv.component.scss']
})
export class NewCvComponent implements OnInit {

  newCvForm: FormGroup;
  educations: FormArray;
  courses: FormArray;
  skills: FormArray;
  places = [
    {label: 'Afghanistan', value: 'afghanistan'},
    {label: 'Argentina', value: 'argentina'},
    {label: 'Croatia', value: 'croatia'},
    {label: 'Egypt', value: 'egypt'},
    {label: 'Italy', value: 'italy'},
    {label: 'Romania', value: 'romania'},
    {label: 'Russia', value: 'russia'},
    {label: 'United States of America', value: 'united_states_of_america'},
    {label: 'United Kingdom', value: 'united_kingdom'}
  ];

  Citizenships = [
    {label: 'Afghanistan', value: 'daml:AF'},
    {label: 'Argentina', value: 'daml:AR'},
    {label: 'Croatia', value: 'daml:HR'},
    {label: 'Egypt', value: 'daml:EG'},
    {label: 'Italy', value: 'daml:IT'},
    {label: 'Romania', value: 'daml:RO'},
    {label: 'Russia', value: 'daml:RU'},
    {label: 'United States of America', value: 'daml:US'},
    {label: 'United Kingdom', value: 'daml:GB'},
  ]

  maritalStatus = [
    {label: 'Married', value: 'base_rdfs:Married'},
    {label: 'Divorced', value: 'base_rdfs:Divorced'},
    {label: 'Single', value: 'base_rdfs:Single'},
    {label: 'Widowed', value: 'base_rdfs:Widowed'}
  ]

  degrees= [
    {label: 'Associate', value: 'associate'},
    {label: 'Bachelor', value: 'bachelor'},
    {label: 'Master', value: 'master'},
    {label: 'Doctoral', value: 'doctoral'}
  ];

  organizations= [];

  skillLevels= [
    {label: 'Beginner', value: 'beginner'},
    {label: 'Average', value: 'average'},
    {label: 'Skilled', value: 'skilled'},
    {label: 'Specialist', value: 'specialist'},
    {label: 'Expert', value: 'expert'}
  ];

  skillTypes = [
    {label: 'Language Skill', value: ':LanguageSkill'},
    {label: 'Programming Skill', value: ':ProgrammingSkill'}
  ];


  constructor(private formBuilder: FormBuilder, 
    private queryService: QueryService,
    private router: Router) {

    this.initilizeForm();

   }

  ngOnInit(): void {
    console.log('ngoninit');
    this.queryService.searchResource('?s', 'rdf:type', ':EducationOrg').subscribe(
      data => {
        console.log(data);
        data.forEach(item => {
          let s = item['s']['value'];

          if( s.search('#') != -1 ){
              s = s.match(/#(.*)/)[1];
          }
          let s_label = s;
          s_label = s_label.replace(/_/g, ' ');
          this.organizations.push({label: s_label, value: s});
        });
      }, err => {
        console.log(err);
      }
    );

  }

  initilizeForm(){
    this.newCvForm = this.formBuilder.group({
      codice_fiscale: new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])),
      name: new FormControl("", Validators.compose([
         Validators.required,
         Validators.minLength(3)
      ])),
      surname: new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])),
      birthDate: new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])),
      birthPlace: new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])),
      citizenship: new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(1)
      ])),
      hasNationality: new FormControl("", Validators.compose([
        Validators.required,
        Validators.minLength(1)
      ])),
      gender: new FormControl("", Validators.compose([
        Validators.required
      ])),
      driven_license: new FormControl("", Validators.compose([
        Validators.required
      ])),
      noOfChildren: new FormControl("", Validators.compose([
        Validators.required
      ])),
      maritalStatus: new FormControl("", Validators.compose([
        Validators.required
      ])),
      title: new FormControl("", Validators.compose([
        Validators.required
      ])),
      description: new FormControl("", Validators.compose([
        Validators.required
      ])),
      active: new FormControl("", Validators.compose([
        Validators.required
      ])),
      confidential: new FormControl("", Validators.compose([
        Validators.required
      ])),
      copyright: new FormControl("", Validators.compose([
        Validators.required
      ])),
      image: new FormControl("", Validators.compose([
        Validators.minLength(3)
      ])),
      educations: this.formBuilder.array([ ]),
      courses: this.formBuilder.array([ ]),
      skills: this.formBuilder.array([ ])
      
    });
  }

  get f() { return this.newCvForm.controls; }
  get e() { return this.f.educations as FormArray; }
  get c() { return this.f.courses as FormArray; }
  get s() { return this.f.skills as FormArray; }

  createEducation(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      degreeType: ['', Validators.required, Validators.email],
      eduDescription: ['', Validators.required],
      eduGradDate: ['', Validators.required],
      eduMajor: [''],
      eduMinor: [''],
      eduStartDate: ['', Validators.required],
      studiedIn: ['', Validators.required]
    });
  }

  createCourses(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      courseTitle: ['', Validators.required, Validators.email],
      courseDescription: ['', Validators.required],
      courseFinishDate: ['', Validators.required],
      courseStartDate: ['', Validators.required],
      courseURL: ['', Validators.required],
      isCertification: [''],
      organizedBy: ['', Validators.required]
    });
  }

  createSkills(): FormGroup {
    return this.formBuilder.group({
      id: [''],
      skillName: ['', Validators.required, Validators.email],
      skillLevel: ['', Validators.required],
      skillYearsExperience: ['', Validators.required],
      skillType: ['', Validators.required],
      skillLastUsed: ['', Validators.required]
    });
  }

  addEducation(){
    this.educations = this.newCvForm.get('educations') as FormArray;
    this.educations.push(this.createEducation());
  }

  addCourse(){
    this.courses = this.newCvForm.get('courses') as FormArray;
    this.courses.push(this.createCourses());
  }

  addSkill(){
    this.skills = this.newCvForm.get('skills') as FormArray;
    this.skills.push(this.createSkills());
  }

  addCv2(value){ 

    console.log(value);
    
  }

  addCv(value){
    if(value.codice_fiscale == ''){
      return false;
    } else {
      let d = value.birthDate.getDate() + '/' + (value.birthDate.getMonth()+1) + '/' + value.birthDate.getFullYear();

      value.codice_fiscale =  value.codice_fiscale.replace(/ /g, "_");
      
      value.educations.forEach(function(item, i) {
        item.id = ':' + value.codice_fiscale + '_e_' + item.degreeType + '_' + i;
        item.eduStartDate = item.eduStartDate.getDate() + '/' + (item.eduStartDate.getMonth()+1) + '/' + item.eduStartDate.getFullYear();
        item.eduGradDate = item.eduGradDate.getDate() + '/' + (item.eduGradDate.getMonth()+1) + '/' + item.eduGradDate.getFullYear();
        item.studiedIn = ':' + item.studiedIn.replace(/ /g, "_");
      });
  
      value.courses.forEach(function(item, i) {
        item.courseTitle = item.courseTitle.toLowerCase().replace(/ /g, "_");
        item.id = ':' + value.codice_fiscale + '_c_' + item.courseTitle + '_' + i;
        item.courseFinishDate = item.courseFinishDate.getDate() + '/' + (item.courseFinishDate.getMonth()+1) + '/' + item.courseFinishDate.getFullYear();
        item.courseStartDate = item.courseStartDate.getDate() + '/' + (item.courseStartDate.getMonth()+1) + '/' + item.courseStartDate.getFullYear();      
        item.isCertification = item.isCertification.toString();
        item.organizedBy = ':' + item.organizedBy.replace(/ /g, "_");
      });
  
      value.skills.forEach( function(item, i) {
        item.skillName = item.skillName.toLowerCase().replace(/ /g, "_");
        item.id = ':' + value.codice_fiscale + '_s_' + item.skillName + '_' + i;
        item.skillLastUsed = item.skillLastUsed.getDate() + '/' + (item.skillLastUsed.getMonth()+1) + '/' + item.skillLastUsed.getFullYear();      
  
      });

      let cv_id = ':' + value.codice_fiscale + '_CV';

      console.log('prima della chiamata');
      console.log(value);

      this.queryService.addPerson(':' + value.codice_fiscale, value.name, value.surname, d, value.birthPlace, value.gender, value.citizenship, value.driven_license.toString(), value.noOfChildren, value.maritalStatus, value.hasNationality, value.image).subscribe(
        data => {

          console.log(data);
          
          this.queryService.addCV(':' + value.codice_fiscale, cv_id, value.active.toString(), value.confidential.toString(), value.copyright.toString(), value.description, value.title, value.educations, value.courses, value.skills).subscribe(
            data => {
              console.log(data);
              this.router.navigate(['/home']);
            }, err => {
              console.log(err);
            }
          );

        }, err => {
          console.log('errore');
          console.log(err);
        }
      )

    }
    
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import { CvDataComponent } from './cv-data/cv-data.component';
import { SparqlComponent } from './sparql/sparql.component';
import { NewCvComponent } from './new-cv/new-cv.component';

const routes: Routes = [
    {
        path: "",
        component: HomeComponent

    },
    {
        path: "home",
        component: HomeComponent

    },
    {
        path: "sparql",
        component: SparqlComponent

    },
    {
        path: "cv/data/:id",
        component: CvDataComponent,
        runGuardsAndResolvers: 'paramsOrQueryParamsChange'

    },
    {
        path: "newCV",
        component: NewCvComponent
    },
    {
        path: "**",
        redirectTo: '/'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  items: MenuItem[];

  ngOnInit() {
    this.items = [
      {
          label: 'Home',
          routerLink: ['/home'],
      },
      {
        label: 'Sparql',
        routerLink: ['/sparql'],
      },
      {
        label: 'Nuovo CV',
        icon: 'pi pi-fw pi-plus',
        routerLink: ['/newCV']
      }
    ]
  }


}

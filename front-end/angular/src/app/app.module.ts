import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import {HttpClientModule} from "@angular/common/http";

import {InputTextareaModule} from 'primeng/inputtextarea';
import {ButtonModule} from 'primeng/button';
import {MenubarModule} from 'primeng/menubar';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {CardModule} from 'primeng/card';
import {TableModule} from 'primeng/table';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {CheckboxModule} from 'primeng/checkbox';
import {RadioButtonModule} from 'primeng/radiobutton';
import {CalendarModule} from 'primeng/calendar';
import {FileUploadModule} from 'primeng/fileupload';

import { QueryService } from './services/query';
import { HttpModule } from '@angular/http';
import { CvDataComponent } from './cv-data/cv-data.component';
import { SharedService } from './services/shared';
import { SparqlComponent } from './sparql/sparql.component';
import { NewCvComponent } from './new-cv/new-cv.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        CvDataComponent,
        SparqlComponent,
        NewCvComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        HttpModule,
        InputTextareaModule,
        ButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MenubarModule,
        AutoCompleteModule,
        CardModule,
        TableModule,
        InputTextModule,
        DropdownModule,
        CheckboxModule,
        RadioButtonModule,
        CalendarModule,
        FileUploadModule
    ],
    providers: [
        QueryService,
        SharedService
    ],
    bootstrap: [AppComponent],
    entryComponents: []
})
export class AppModule {
}
